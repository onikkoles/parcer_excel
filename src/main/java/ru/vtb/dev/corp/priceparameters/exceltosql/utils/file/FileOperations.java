package ru.vtb.dev.corp.priceparameters.exceltosql.utils.file;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.util.Strings;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class FileOperations {
    private final String fileExt;
    private final String folderPath;

    public FileOperations(@Value("${srcFileExt}") String fileExt,
                          @Value("${srcFolderPath}") String folderPath) {
        this.fileExt = fileExt;
        this.folderPath = folderPath;
    }

    @Data
    @Builder
    @ToString
    public static class FileDescription {
        private String fileNameWithPath;
        private String fileName;
        private String fileExtension;

        public String getFileExtensionNoDot() {
            return fileExtension == null
                    ? null
                    : fileExtension.replace(".", Strings.EMPTY);
        }
    }

    public static List<FileDescription> getFilesInFolder(String path, String fileExt) {
        if (path == null || path.isBlank()) {
            log.error("Folder path is null or blank");
            return Collections.emptyList();
        }

        if (!directoryCanRead(path)) {
            log.error("Folder {} cannot be read", path);
            return Collections.emptyList();
        }

        try (Stream<Path> pathStream = Files.find(Paths.get(path), Integer.MAX_VALUE, (p, a) -> a.isRegularFile())) {
            final List<FileDescription> fileDescriptions = pathStream
                    .filter(f -> fileExt == null || f.getFileName().toString().toLowerCase().endsWith(fileExt))
                    .map(filename -> FileDescription.builder().fileNameWithPath(filename.toAbsolutePath().toString())
                            .fileName(filename.getFileName().toString())
                            .fileExtension(FilenameUtils.getExtension(filename.getFileName().toString()).toLowerCase())
                            .build())
                    .collect(Collectors.toList());
            return fileDescriptions;
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            return Collections.emptyList();
        }
    }

    public static boolean directoryCanRead(String path) {
        if (path == null) {
            return false;
        }
        final File directory = new File(path);
        return directory.exists() && directory.canRead();
    }

    public static void deleteFile(String fileName){
        File file = new File(fileName);
        if(file.delete()){
            log.info("Удален файл " + fileName);
        }
    }

}
