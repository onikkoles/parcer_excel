package ru.vtb.dev.corp.priceparameters.exceltosql.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class SqlWriter {
    private static final String CREDIT_SUM_MIN = "credit_sum_min";
    private static final String CREDIT_SUM_MAX = "credit_sum_max";
    private static final String CREDIT_PERIOD_MAX_DAYS = "credit_period_max_days";
    private static final String CREDIT_PERIOD_MIN_DAYS = "credit_period_min_days";
    private static final String CREDIT_PERIOD_MAX_MONTHS = "credit_period_max_months";
    private static final String CREDIT_PERIOD_MIN_MONTHS = "credit_period_min_months";
    private static final String BASE_INTEREST_RATE_AMOUNT = "base_interest_rate_amount";
    private static final String PRICE_PARAMETER_ID = "price_parameter_id";
    private static final String SUBPRODUCT_ID = "subproduct_id";
    private static final String SUBPRODUCT_CODE = "subproduct_code";
    private static final String CURRENCY_ID = "currency_id";
    private static final String CURRENCY_UUID = "58d96000-0ecb-41c1-b118-6a22bc40a3c7";

    public void write(String fileName, HashMap<Integer, String> headers, List<List<String>> allRows) {
        checkSubproductIdCode(headers, allRows);
        List<String> listCredit = getChangeMinMax(headers, allRows);
        System.out.println(listCredit);
        List<String> listSetDelete = getSetDelete(headers, allRows);
        List<String> listSetInsert = getInsertPriceParameter(headers, allRows);

        try (final PrintWriter writer = new PrintWriter(new FileOutputStream(fileName))) {
            for (String element : listSetDelete)
                writer.println(element);
            for (String element : listSetInsert)
                writer.println(element);
        } catch ( FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public List<String> getChangeMinMax(HashMap<Integer, String> headers, List<List<String>> allRows) {
        List<String> changeMinMax = new ArrayList<>();
        String url = "jdbc:postgresql://localhost:5432/spo";
        String username = "postgres";
        String password = "postgres";

        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            Statement stmt = conn.createStatement();
            for (List<String> row : allRows) {
                String sql = "SELECT credit_sum_min, credit_sum_max, price_parameter_id FROM price_parameter where price_parameter_id='"
                        + row.get(getKeyByValue(headers, PRICE_PARAMETER_ID)) + "'";
                ResultSet rs = stmt.executeQuery(sql);
                while (rs.next()) {
                    Double credit_sum_min_dbd = Double.parseDouble(rs.getString("credit_sum_min"));
                    Integer credit_sum_min_db = credit_sum_min_dbd.intValue();
                    Double credit_sum_max_dbd = Double.parseDouble(rs.getString("credit_sum_max"));
                    Integer credit_sum_max_db = credit_sum_max_dbd.intValue();
                    Double credit_sum_min_exceld = Double.parseDouble(row.get(getKeyByValue(headers, CREDIT_SUM_MIN)));
                    Integer credit_sum_min_excel = credit_sum_min_exceld.intValue();
                    Double credit_sum_max_exceld = Double.parseDouble(row.get(getKeyByValue(headers, CREDIT_SUM_MAX)));
                    Integer credit_sum_max_excel = credit_sum_max_exceld.intValue();

                    int minb = credit_sum_min_db.compareTo(credit_sum_min_excel);
                    int maxb = credit_sum_max_db.compareTo(credit_sum_max_excel);
                    if ((minb != 0) || (maxb !=0)) {
                        log.error("credit_sum_min_db: " + credit_sum_min_db);
                        log.error("credit_sum_min_excel: " + credit_sum_min_excel);
                        log.error("credit_sum_max_db: " + credit_sum_max_db);
                        log.error("credit_sum_max_excel: " + credit_sum_max_excel);

                        String str = rs.getString("price_parameter_id");
                        log.error("price_parameter_id: " + str);
                    }
                }
                rs.close();
            }
            stmt.close();
            conn.close();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return changeMinMax;
    }

    public void checkSubproductIdCode(HashMap<Integer, String> headers, List<List<String>> allRows) {
        String url = "jdbc:postgresql://localhost:5432/spo";
        String username = "postgres";
        String password = "postgres";

        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            Statement stmt = conn.createStatement();
            int countId = 0;
            int countCode = 0;
            int i = 0;
            for (List<String> row : allRows) {
                i++;
                String subproduct_id_excel = row.get(getKeyByValue(headers, SUBPRODUCT_ID));
                String sql = "SELECT s.subproduct_code, s.subproduct_id  from subproduct s " +
                        "inner join status s2 on s2.status_id = s.status_id " +
                        "where s2.status_name='ACTIVATED' and " +
                        "s.subproduct_id='" + subproduct_id_excel + "'";
                ResultSet rs = stmt.executeQuery(sql);
                String subproduct_code_excel =  row.get(getKeyByValue(headers, SUBPRODUCT_CODE));
                while (rs.next()) {
                    String subproduct_code_db = rs.getString("subproduct_code");
                    if (!subproduct_code_db.equals(subproduct_code_excel)) {
                        countCode++;
                        log.error("1-subproduct_code_excel: " + subproduct_code_excel);
                        log.error("1-subproduct_code_db: " + subproduct_code_db);
                        log.error("1-subproduct_id_excel: " + subproduct_id_excel);
                    }
                }
                rs.close();

                String sql2 = "SELECT subproduct_code, subproduct_id  from subproduct s " +
                                "inner join status s2 on s2.status_id = s.status_id " +
                                "where s2.status_name='ACTIVATED' and " +
                                "subproduct_code='"+ subproduct_code_excel + "'";
                ResultSet rs2 = stmt.executeQuery(sql2);
                while (rs2.next()) {
                    String subproduct_id_db = rs2.getString("subproduct_id");
                    if (!subproduct_id_db.equals(subproduct_id_excel)) {
                        countId++;
                        log.error("2-subproduct_code_excel: " + subproduct_code_excel);
                        log.error("2-subproduct_id_db: " + subproduct_id_db);
                        log.error("2-subproduct_id_excel: " + subproduct_id_excel);
                        log.error("================= - " + i);
                    }
                }
            }
            log.error("!=========CODE============ - " + countCode);
            log.error("!==========ID========== - " + countId);
            stmt.close();
            conn.close();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    public List<String> getSetDelete(HashMap<Integer, String> headers, List<List<String>> allRows) {
        String url = "jdbc:postgresql://localhost:5432/spo";
        String username = "postgres";
        String password = "postgres";

        List<String> updateList = new ArrayList<>();
        String nameColumn = "subproduct_id";
        updateList.add(
                "UPDATE price_parameter SET deleted = TRUE\n" +
                        "WHERE " + nameColumn + " IN \n" +
                        "(");
        int count = 0;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            Statement stmt = conn.createStatement();

            for (List<String> row : allRows) {
                count++;
                String subproduct_code_excel = row.get(getKeyByValue(headers, SUBPRODUCT_CODE));

                String sql = "SELECT subproduct_code, subproduct_id  from subproduct s " +
                        "inner join status s2 on s2.status_id = s.status_id " +
                        "where s2.status_name='ACTIVATED' and " +
                        "subproduct_code='" + subproduct_code_excel + "'";

                ResultSet rs = stmt.executeQuery(sql);
                while (rs.next()) {
                    String subproduct_id = rs.getString("subproduct_id");
                    if (allRows.size() == count) {
                        updateList.add("    '" + subproduct_id + "'");
                    } else {
                        updateList.add("    '" + subproduct_id + "',");
                    }
                }
                rs.close();

            }
            updateList.add(");");
            stmt.close();
            conn.close();

            return updateList;

        } catch (Exception e) {
            log.error(e.getMessage());
        return null;
        }
    }

    public List<String> getInsertPriceParameter(HashMap<Integer, String> headers, List<List<String>> allRows) {
        List<String> insertList = new ArrayList<>();

        String url = "jdbc:postgresql://localhost:5432/spo";
        String username = "postgres";
        String password = "postgres";

        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            Statement stmt = conn.createStatement();


            insertList.add("");
            insertList.add("INSERT INTO price_parameter (");
            insertList.add("    " +
                    "price_parameter_id, " +
                    "currency_id, " +
                    "subproduct_id, " +
                    "base_interest_rate_amount, " +
                    "credit_sum_min, " +
                    "credit_sum_max, " +
                    "credit_period_min_months, " +
                    "credit_period_max_months, " +
                    "credit_period_min_days, " +
                    "credit_period_max_days");
            insertList.add(") ");
            insertList.add("VALUES ");

            for(List<String> row :  allRows) {

                UUID PriceParameterUUID = UUID.randomUUID();

                String strValue = "  ('" + PriceParameterUUID + "'";
                strValue = strValue + ", '" + CURRENCY_UUID + "'";

                String subproduct_code_excel = row.get(getKeyByValue(headers, SUBPRODUCT_CODE));

                String sql = "SELECT subproduct_code, subproduct_id  from subproduct s " +
                        "inner join status s2 on s2.status_id = s.status_id " +
                        "where s2.status_name='ACTIVATED' and " +
                        "subproduct_code='" + subproduct_code_excel + "'";

                String subproduct_id_db = "";
                ResultSet rs = stmt.executeQuery(sql);
                while (rs.next()) {
                    if (!subproduct_id_db.isEmpty()) {
                        log.error("!!!!!!!!!!!!!!!!!!_____________________!!!!!!!!!!!!!!!!!!!!!!!!!");
                        log.error(subproduct_id_db);
                    }
                    subproduct_id_db = rs.getString("subproduct_id");
                }

                if (subproduct_id_db.isEmpty()) {
                    log.error("!!!!!!!!!!!!!!!!!!_______subproduct_code_excel______________!!!!!!!!!!!!!!!!!!!!!!!!!");
                    log.error(subproduct_code_excel);
                } else {

//                strValue = strValue + ", '" + row.get(getKeyByValue(headers, SUBPRODUCT_ID)) + "'";
                    strValue = strValue + ", '" + subproduct_id_db + "'";

                    double d = Double.parseDouble(row.get(getKeyByValue(headers, BASE_INTEREST_RATE_AMOUNT)));
                    DecimalFormat f = new DecimalFormat("##.00");
                    String strRate = f.format(d).replaceAll(",", ".");
                    if (d == 0) {
                        strRate = "0";
                    }
                    strValue = strValue + ", " + strRate;

                    strValue = strValue + ", '" + row.get(getKeyByValue(headers, CREDIT_SUM_MIN)) + "'";
                    strValue = strValue + ", '" + row.get(getKeyByValue(headers, CREDIT_SUM_MAX)) + "'";
                    strValue = strValue + ", '" + row.get(getKeyByValue(headers, CREDIT_PERIOD_MIN_MONTHS)) + "'";
                    strValue = strValue + ", '" + row.get(getKeyByValue(headers, CREDIT_PERIOD_MAX_MONTHS)) + "'";
                    strValue = strValue + ", '" + row.get(getKeyByValue(headers, CREDIT_PERIOD_MIN_DAYS)) + "'";
                    strValue = strValue + ", '" + row.get(getKeyByValue(headers, CREDIT_PERIOD_MAX_DAYS)) + "'";

                    if (allRows.indexOf(row) + 1 == allRows.size()) {
                        insertList.add(strValue + ");");
                    } else {
                        insertList.add(strValue + "),");
                    }
                }
            }
            return insertList;
        }
        catch (Exception e) {
            log.error("Ошибка создания файла скрипта");
            return null;
        }
    }

    public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
        for (Map.Entry<T, E> entry : map.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }
}
