package ru.vtb.dev.corp.priceparameters.exceltosql.utils.file;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class InputFile {

    private List<String> header;
    private List<List<String>> rows;
}
