package ru.vtb.dev.corp.priceparameters.exceltosql.utils.file;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public interface FileFormat {
    InputFile loadFile(String fileName);

    default boolean checkSubproductCodeID(List<List<String>> rows) {
        HashMap<String, String> mapSubproduct = new HashMap<>();
        try {
            for (int i = 0; i < rows.size(); i++) {
                if (mapSubproduct.containsKey(rows.get(i).get(0))) {
                    if (!mapSubproduct.get(rows.get(i).get(0)).equals(rows.get(i).get(10))) {
                        System.out.println("subproduct_id - " + rows.get(i).get(10) +
                                ", subproduct_code - " + rows.get(i).get(0) +
                                "  " + mapSubproduct.get(rows.get(i).get(0)));
                        return false;
                    }
                } else {
                    mapSubproduct.put(rows.get(i).get(0), rows.get(i).get(10));
                }
            }
        }
        catch(Exception ex) {
            System.out.println(ex);
            return false;
        }
        return true;
    }
}
