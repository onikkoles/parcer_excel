package ru.vtb.dev.corp.priceparameters.exceltosql.utils.file;

import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Component
@ConditionalOnProperty(name = "srcFileExt", havingValue = "csv")
public class FileCSVImpl implements FileFormat{
    private static final String QUOTE = "\"";
    private static final String DEFAULT_DELIMITER = ";";

    @Override
    public InputFile loadFile(String fileName) {
        if (fileName == null || fileName.isBlank()) {
            log.error("Проблема с именем файла");
            return null;
        }
        final String dlm = DEFAULT_DELIMITER;
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String str = reader.readLine();
            final List<String> header = splitString(str, dlm);
            final List<List<String>> rows = new ArrayList<>();
            while((str = reader.readLine()) != null) {
                rows.add(splitString(str, dlm));
            }
            return InputFile.builder().header(header).rows(rows).build();
        } catch (Exception e) {
            log.error("Ошибка загрузки файла CSV");
            return null;
        }
    }

    private static List<String> splitString(String src, String delimiter) {
        final String [] cols = src.split(delimiter, -1);
        for (String value : cols) {
            if (value != null && value.length() > 1 && value.startsWith(QUOTE) && value.endsWith(QUOTE)) {
                value = (value.length() == 2) ? Strings.EMPTY : value.substring(0, value.length() - 1);
            }
        }
        return Arrays.asList(cols);
    }

}
