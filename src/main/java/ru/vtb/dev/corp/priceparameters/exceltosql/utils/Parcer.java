package ru.vtb.dev.corp.priceparameters.exceltosql.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.vtb.dev.corp.priceparameters.exceltosql.utils.file.FileFormat;
import ru.vtb.dev.corp.priceparameters.exceltosql.utils.file.FileOperations;
import ru.vtb.dev.corp.priceparameters.exceltosql.utils.file.InputFile;

import java.util.HashMap;
import java.util.List;

@Slf4j
@Component
public class Parcer {
    private final String folderPath;
    private final String fileExt;
    private final String outFileName;

    private final FileFormat fileFormat;
    private final SqlWriter sqlWriter;

    public Parcer(@Value("${srcFolderPath}") String folderPath,
                  @Value("${srcFileExt}") String fileExt,
                  @Value("${outFileName}") String outFileName,
                  FileFormat fileFormat,
                  SqlWriter sqlWriter) {
        this.fileExt = fileExt;
        this.folderPath = folderPath;
        this.outFileName = outFileName;
        this.fileFormat = fileFormat;
        this.sqlWriter = sqlWriter;
    }


    public void parse() {
        log.info("Старт");
        try {
            process();
        } catch (Exception e) {
            log.error("Ошибка: " + e.getLocalizedMessage(), e);
        }
        log.info("Стоп");
    }

    private void process() {
        final List<FileOperations.FileDescription> filesInFolder = FileOperations.getFilesInFolder(
                this.folderPath,
                this.fileExt
        );

        Integer count = 0;
        for (FileOperations.FileDescription file : filesInFolder) {
            try {
                InputFile inputFile = fileFormat.loadFile(file.getFileNameWithPath());
                if (inputFile != null && inputFile.getHeader() != null && inputFile.getRows() != null) {
                    final HashMap<Integer, String> headerToIndex = mapHeaderToIndex(inputFile.getHeader());
                    if (count == 0){
                        FileOperations.deleteFile(outFileName + ".sql");
                        sqlWriter.write(outFileName + ".sql" , headerToIndex, inputFile.getRows());
                    }
                    else {
                        FileOperations.deleteFile(outFileName + "_" + count.toString() + ".sql");
                        sqlWriter.write(outFileName + "_" + count.toString() + ".sql", headerToIndex, inputFile.getRows());
                    }
                    count++;
                }
            } catch (Exception e) {
                log.error("Ошибка: " + e.getLocalizedMessage(), e);
            }
        }
        log.info(filesInFolder.size() + " файлов обработано");
    }

    private HashMap<Integer, String> mapHeaderToIndex(final List<String> headers) {
        HashMap<Integer, String> map = new HashMap<>();

        for (int i = 0; i < headers.size() ; i++) {
            if (headers.get(i) != null) {
                map.put(i, headers.get(i).toLowerCase());
            }
        }

        return map;
    }

}
