package ru.vtb.dev.corp.priceparameters.exceltosql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.vtb.dev.corp.priceparameters.exceltosql.utils.Parcer;

@SpringBootApplication
public class PriceParametersExcelToSqlApplication implements CommandLineRunner {

    private final Parcer parcer;

    public PriceParametersExcelToSqlApplication(Parcer parcer) {
        this.parcer = parcer;
    }

    public static void main(String[] args) {
        SpringApplication.run(PriceParametersExcelToSqlApplication.class, args);

    }

    @Override
    public void run(String... args) {
        parcer.parse();
    }
}
