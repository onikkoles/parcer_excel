package ru.vtb.dev.corp.priceparameters.exceltosql.utils.file;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.NumberToTextConverter;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@ConditionalOnProperty(name = "srcFileExt", havingValue = "xlsx")
public class FileExcelImpl implements FileFormat{

    private static final int max_column = 10;

    @Override
    public InputFile loadFile(String fileName) {
        if (fileName == null || fileName.isBlank()) {
            log.error("Проблема с именем файла");
            return null;
        }
        try (XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(fileName))) {
            XSSFSheet sheet = workbook.getSheetAt(0);
            if (sheet.getPhysicalNumberOfRows() == 0) {
                return null;
            }

            Integer startRow = 1;
            final List<String> header = new ArrayList<>();
            final Row headerRow = sheet.getRow(0);
            if (headerRow != null) {
                for (int c = 0; c < headerRow.getLastCellNum(); c++) {
                    header.add(getCellValue(headerRow.getCell(c)));
                }
            }

            final List<List<String>> rows = new ArrayList<>();
            for (int r = startRow; r <= sheet.getLastRowNum(); r++) {
                final Row row = sheet.getRow(r);
                if (row != null) {
                    final List<String> cols = new ArrayList<>();
                    for (int c = 0; c < row.getLastCellNum(); c++) {
                        cols.add(getCellValue(row.getCell(c)));
                    }
                    rows.add(cols);
                }
            }

//            if (!checkSubproductCodeID(rows)) {
//                log.error("subproduct_id встречается повторно с разными subproduct code - в файле" + fileName);
//                return null;
//            }

            return InputFile.builder().header(header).rows(rows).build();
        } catch (Exception e) {
            log.error("Ошибка загрузки файла XLSX - " + fileName);
            log.error(e.getMessage());
            return null;
        }
    }

    private static String getCellValue(Cell cell) {
        if (cell == null) {
            return null;
        }
        switch (cell.getCellType()) {
            case NUMERIC:
                return NumberToTextConverter.toText(cell.getNumericCellValue());
            case STRING:
                return cell.getStringCellValue().replaceAll("\\s+","").replaceAll("'", "");
//                return cell.getStringCellValue();
            case FORMULA:
                return null;
            case BLANK:
                return null;
            case BOOLEAN:
                return String.valueOf(cell.getBooleanCellValue());
            case ERROR:
                return null;
            default:
                return null;
        }
    }

}
